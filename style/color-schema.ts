export const darkColorSchema = {
  main: '#5E5757',
  complementary: '#302929',
  text: '#806969'
}
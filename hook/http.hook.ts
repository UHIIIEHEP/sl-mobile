import axios from "axios";

const httpHook = (url: string, method: string, payload: object = {}): Promise<any> => {
    const settings: any = {
      url,
      method,
      data: payload,
    }

    return axios.request(settings)
      .then(function (resp: any) {
          return resp;
      })
      .catch(function (error) {
          console.log({error});
      });
}

export default httpHook;

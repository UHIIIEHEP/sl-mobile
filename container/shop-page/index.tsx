import React from 'react';

import {
  View,
} from "react-native";

import ShopBlock from "../../component/shop-block";

const ShopPage = () => {
  return (
    <View>
      <ShopBlock />
    </View>
  )
}

export default ShopPage;

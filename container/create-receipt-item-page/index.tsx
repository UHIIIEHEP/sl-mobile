import React from 'react';

import {
  View,
} from "react-native";

import CreateReceiptItem from "../../component/create-receipt-item";

const CreateReceiptItemPage = () => {
  return (
    <View>
      <CreateReceiptItem />
    </View>
  )
}

export default CreateReceiptItemPage;

import React from 'react';

import {
  View,
} from "react-native";

import CreateReceipt from "../../component/create-receipt";

const CreateReceiptPage = () => {
  return (
    <View>
      <CreateReceipt />
    </View>
  )
}

export default CreateReceiptPage;

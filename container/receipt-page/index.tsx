import React from 'react';

import {
  View,
} from "react-native";
import ReceiptBlock from "../../component/receipt-block";


const ReceiptPage = () => {
  return (
    <View>
      <ReceiptBlock />
    </View>
  )
}

export default ReceiptPage;

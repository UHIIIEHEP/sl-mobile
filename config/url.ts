const baseUrl = 'http://192.168.0.4:5000'

export const API_SHOP_LIST_GET = `${baseUrl}/shop/list`;
export const API_SHOP_CATEGORY_LIST_GET = `${baseUrl}/shop/category/list`;
export const API_RECEIPT_LIST_GET = `${baseUrl}/shop/receipt/list`;
export const API_RECEIPT_CREATE_POST = `${baseUrl}/shop/receipt/create`;
export const API_UNIT_LIST_GET = `${baseUrl}/unit/list`;

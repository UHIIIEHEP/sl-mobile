export const menuItems: any = {
  receipt: [
    'receipt',
    'add_receipt',
    'shop',
  ],
  shop: [
    'receipt',
    'shop',
  ],
  add_receipt: [
    'receipt',
    'addReceiptItem',
    'shop',
  ],
  addReceiptItem: [
  ],
};

export const menuElement: any ={
  receipt: {
    name: 'receipt',
    alias: 'receipt',
    icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
  add_receipt: {
    name: 'add',
    alias: 'add_receipt',
    icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
  addReceiptItem: {
    name: 'add',
    alias: 'addReceiptItem',
    icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
  shop: {
    name: 'shop',
    alias: 'shop',
    icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
  receiptItemCancel: {
    name: 'Cancel',
    alias: 'add_receipt',
    // icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
  receiptItemAddItem: {
    name: 'Add',
    alias: 'add_receipt',
    // icon: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/receipt-2910005-2421560.png',
  },
}

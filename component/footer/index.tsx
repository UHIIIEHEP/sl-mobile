import React from 'react';

import {
  View,
  StyleSheet,
  Pressable,
  Text,
} from 'react-native';

import {useDispatch, useSelector} from "react-redux";
import {darkColorSchema} from "./../../style/color-schema";
import {IStateCommonReducer} from "./../../reducers/interface";
import {setActualScreen} from "../../reducers/commonReducer";
import {menuElement, menuItems} from "../../config/menu";
import {setActualShop} from "../../reducers/shopReducer";

const Footer = () => {

  const dispatch = useDispatch();
  const actualScreen = useSelector((state: IStateCommonReducer) => state.commonReducer.actualScreen)
  const setActualScreenAction = (screen: string) => {
    switch (screen) {
      case 'receipt':
        return ([
          dispatch(setActualScreen(screen)),
          dispatch(setActualShop(null)),

        ])

      default:
        return dispatch(setActualScreen(screen));
    }
  };

  return (
    <View style={styles.footer}>
      {
        menuItems[actualScreen].map((item: any, index: number) => {
          return (
            <Pressable
              style={styles.button}
              onPress = {() => (setActualScreenAction(menuElement[item].alias))}
              key = {index}
            >
              <View>
                <Text
                  style={
                    [styles.text, {
                      transform: [{translateY: 25}]
                    }]
                  }>
                  {menuElement[item].name}
                </Text>
              </View>
            </Pressable>
          )
        })
      }
    </View>
  )
}

const size = 70;

const styles = StyleSheet.create({
  footer: {
    backgroundColor: darkColorSchema.complementary,
    height: '10%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  button: {
    height: size,
    width: size,
    marginLeft: 10,
    marginRight: 10,
    position: 'relative',
  },

  button_menu: {

  },
  text: {
    textAlign: 'center',
    width: '100%',
    fontWeight: 'bold',
    position: 'absolute',
    top: '50%',
    color: darkColorSchema.text,
  },
});

export default Footer;

import React from 'react';

import {
  View,
  StyleSheet,
  Pressable,
  Text,
  TextInput,
} from 'react-native';

import {darkColorSchema} from "../../style/color-schema";
import {useSelector} from "react-redux";
import {IStateCommonReducer} from "../../reducers/interface";

const Header = () => {
  const visibleInputSearch = useSelector((state: IStateCommonReducer) => state.commonReducer.visibleInputSearch)

  return (
    <View style={styles.header}>
      <View
        style = {styles.inputWrapper}
      >
        {visibleInputSearch &&
        <TextInput
          style = {styles.textInput}
          placeholder = 'Search'
        />
        }
      </View>

      <Pressable
        style={styles.button}
        // onPress = {() => (setActualScreenAction(item.name))}
      >
        <View>
          <Text
            style={
              [styles.text, {
                transform: [{translateY: 25}]
              }]
            }>
            FILTER
          </Text>
        </View>
      </Pressable>

    </View>
  )
}

const size = 70;

const styles = StyleSheet.create({
  header: {
    backgroundColor: darkColorSchema.complementary,
    height: '10%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '2%',
  },
  inputWrapper: {
    flex: 10,
  },
  textInput: {
  },
  button: {
    flex: 2,
    // height: size,
    // width: size,
    // marginLeft: 10,
    // marginRight: 10,
    position: 'relative',
    borderWidth: 1,
    borderColor: 'green',
  },
  text: {
    textAlign: 'center',
    width: '100%',
    fontWeight: 'bold',
    // position: 'absolute',
    // top: '50%',
    color: darkColorSchema.text,
  },
});

export default Header;

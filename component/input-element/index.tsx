import React, {useState} from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Switch,
  Text,
} from "react-native";
import {darkColorSchema} from "../../style/color-schema";
import BouncyCheckbox from "react-native-bouncy-checkbox";

interface IPropsElement {
  title: string,
  type: string,
  onChangeText: any,
}

const InputElement = (props: IPropsElement) => {

  const [inputValue, setInputValue] = useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  switch (props.type) {
    case 'switch':
      return (
        <View
          style = {[
            styles.block
          ]}
        >
          <Text
            style={styles.text}
          >{props.title}</Text>
          <View
            style={styles.input}
          >
            <View
              style={styles.wrap_switch}
            >
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>

        </View>
      )
      break;

    case 'radio':
      return (
        <View>
          <BouncyCheckbox
            text='kg'
          />
          <BouncyCheckbox>2</BouncyCheckbox>
        </View>
      )
    break;

    case 'number':
    default:
      return (
        <View
          style = {[
            styles.block
          ]}
        >
          <Text
            style={styles.text}
          >{props.title}</Text>
          <View
            style={styles.input}
          >
            <TextInput
              style={styles.textInput}
              keyboardType={props.type == 'number' ? 'numeric' : 'default'}
              onChangeText={props.onChangeText(inputValue)}
              value={inputValue}
            ></TextInput>
          </View>

        </View>
      )
  }

}

const styles = StyleSheet.create({
  block: {
    margin: 3,
    height: 50,
    display: 'flex',
    flexDirection: 'row',
  },
  text: {
    width: '20%',
    textAlign: 'left',
    position: 'relative',
    lineHeight: 50,
  },
  input: {
    // flex: 9,
    width: '80%',
    alignContent: "flex-start",
    justifyContent: 'center',

  },
  textInput: {
    borderColor: 'white',
    backgroundColor: darkColorSchema.complementary,
    height: 40,
    borderRadius: 5,
  },

  wrap_switch: {
    // left: 0,
    position: 'absolute',
    borderWidth: 1,
    borderColor: 'green',
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
})

export default InputElement;

/*



        <View
          style = {styles.block}
        >
          <View
            style = {styles.block_input_title}
          >
            <Text
            >
              {props.title}
            </Text>
          </View>
          <View
            style = {styles.block_input}
          >
            <TextInput
              placeholder={props.title}
            />
          </View>
        </View>


 */
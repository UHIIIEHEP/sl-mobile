import React, {useEffect, useState} from 'react';
import {
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {IStateReceiptReducer, IStateShopReducer} from "../../reducers/interface";
import ReceiptItem from "../receipt-item";
import DropDownPicker from "react-native-dropdown-picker";
import {darkColorSchema} from "../../style/color-schema";
import {asyncGetShopCategoryList} from "../../reducers/shopReducer";
import {addProduct, asyncPostReceiptCreate, clearDraftReceipt} from "../../reducers/receiptReducer";


const CreateReceipt = () => {
  const dispatch = useDispatch();

  const shopCategoryAll = useSelector((state: IStateShopReducer) => state.shopReducer.categoryList);
  const actualCategory = useSelector((state: IStateShopReducer) => state.shopReducer.actualCategory);
  const totalPrice = useSelector((state: IStateReceiptReducer) => state.receiptReducer.totalPrice);
  const draftReceipt = useSelector((state: IStateReceiptReducer) => state.receiptReducer.draftReceipt);

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(actualCategory?.value||null);
  const [items, setItems] = useState(shopCategoryAll.map((item: any) => {
    return {
      label: item.name,
      value: item.value,
    }
  }));

  const shopCategoryList = () => dispatch(asyncGetShopCategoryList());
  const clearReceipt = () => dispatch(addProduct([]));

  const receiptCreate = () => dispatch(asyncPostReceiptCreate({
      category: shopCategoryAll[value].name,
      date: new Date(),
      total_price: 0,
      product: draftReceipt,
    }));

  const saveReceipt = async () => {
    receiptCreate();
    clearReceipt();
  }

  useEffect(() => {
    shopCategoryList();
  }, [])

  return (
    <View style={styles.content}>
      <View style={styles.shop_block}>
        <Text style={styles.text}>Категория:</Text>
        <DropDownPicker
          style = {styles.dd_picker}
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
        />
      </View>


      <View>
        <View>
          <Text>Product:</Text>
          <Text>Total:  {totalPrice}</Text>
        </View>
        <ScrollView style={styles.product_list}>
          <View style={styles.content_product_list}>
            {
              draftReceipt.map((item: any, index: number) => {
                return (
                  <ReceiptItem data={item} key={index}/>
                )
              })
            }
          </View>
        </ScrollView>
      </View>

      <Pressable
        style={styles.button}
        onPress = {() => saveReceipt()}
      >
        <Text style={[styles.text_button, styles.button_add]}>Сохранить</Text>
      </Pressable>

      <Pressable
        style={styles.button}
        onPress = {() => clearReceipt()}
      >
        <Text style={[styles.text_button, styles.button_add]}>Отмена</Text>
      </Pressable>

    </View>
  )
}

const styles = StyleSheet.create({
  content: {
    width: '100%',
  },
  shop_block: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 15,
  },
  dd_picker: {
    width: '60%',
  },
  text: {
    textAlignVertical: "center",
    padding: 3,
    marginRight: 10,
  },
  button: {
    backgroundColor: darkColorSchema.complementary,
    color: darkColorSchema.text,
    height: 40,
    marginTop: 5,
    borderRadius: 5,
  },

  text_button: {
    width: '100%',
    textAlign: 'center',
    lineHeight: 40,
    color: darkColorSchema.text,
  },

  button_add: {
  },

  product_list: {
    height: '60%',
  },
  content_product_list: {
    width: '100%',
  },

});

export default CreateReceipt;

import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  ImageBackground,
  ScrollView,
} from "react-native";
import {IReceipt, IStateReceiptReducer, IStateShopReducer} from "../../reducers/interface";
import {asyncGetReceiptList} from "../../reducers/receiptReducer";
import DropDownPicker from "react-native-dropdown-picker";
import {asyncGetShopCategoryList, asyncGetShopList} from "../../reducers/shopReducer";
import {darkColorSchema} from "../../style/color-schema";


const ReceiptBlock = () => {
  // const shopAll = useSelector((state: IStateShopReducer) => state.shopReducer.shopItems);
  const shopCategoryAll = useSelector((state: IStateShopReducer) => state.shopReducer.categoryList);
  const actualCategory = useSelector((state: IStateShopReducer) => state.shopReducer.actualCategory);

  const shopList = () => dispatch(asyncGetShopList());

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(actualCategory?.value||null);
  const [items, setItems] = useState(shopCategoryAll.map((item: any) => {
    return {
      label: item.name,
      value: item.value,
    }
  }));

  const setDDValue = () => {
  }

  const dispatch = useDispatch();

  const receiptAll = useSelector((state: IStateReceiptReducer) => state.receiptReducer.receiptList);

  const receiptList = () => dispatch(asyncGetReceiptList());
  const shopCategoryList = () => dispatch(asyncGetShopCategoryList());

  const receiptItemClick = (id: string) => {
    console.log({id})
  }


  useEffect(() => {
    receiptList();
    shopList();
    setDDValue();
    shopCategoryList();
  }, [value])

  return (
    <View style = {styles.content}>
      <DropDownPicker
        style = {styles.dd_picker}
        open={open}
        value={value}
        items={items}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setItems}
      />
      <ScrollView>
        {
          receiptAll.map((item: IReceipt, index: number) => {
            // const image = item.shop.image;
            return (
              <Pressable
                style={styles.oneReceipt}
                onPress = {() => receiptItemClick(item._id)}
                key = {index}
              >
                <View style={styles.image_block}>
                  {/*<ImageBackground source={image} style={styles.image}>*/}
                  {/*</ImageBackground>*/}
                </View>
                <View style={styles.text_block}>
                  <Text style={styles.text}>Дата: {item.date}</Text>
                  <Text style={styles.text}>Итого: {item.total_price}</Text>
                </View>

              </Pressable>
            )
          })
        }
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  content: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: '100%',
  },
  dd_picker: {
    marginBottom: 15,
  },
  block: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  oneReceipt: {
    height: 100,
    marginBottom: 10,
    borderColor: 'red',
    borderWidth: 1,
    display: 'flex',
    flexDirection: 'row',
  },
  image_block: {
    flex: 3,
  },
  text_block: {
    flex: 9,
    height: 100,
    padding: '3%',
  },

  text: {},
  image: {
    height: 100,
    borderRadius: 55,
  },
});

export default ReceiptBlock;

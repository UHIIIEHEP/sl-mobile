import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import {useSelector} from "react-redux";
import {IStateUnitReducer} from "../../reducers/interface";

const ReceiptItem = (props: any) => {

  const unitAll = useSelector((state: IStateUnitReducer) => state.unitReducer.unitList);

  const unit = unitAll.filter((item: {value: string}) => item.value === props.data.unit)

  return (
    <View style={styles.block}>
      <View
        style = {styles.infoBlock}
      >
        <Text>Name: {props.data.name}</Text>
        <Text>Value: {props.data.value} {unit[0]?.name || ''} {String(props.data.strategy)}</Text>
      </View>

      <View
        style = {styles.priceBlock}
      >
        <Text>{props.data.price}</Text>
      </View>
    </View>
  )

}

const styles = StyleSheet.create({
  block: {
    marginTop: 5,
    padding: 7,
    display: 'flex',
    flexDirection: 'row',

    borderColor: 'green',
    borderWidth: 1,
    borderRadius: 8,
  },
  infoBlock: {
    flex: 9,
  },
  priceBlock: {
    flex: 3,
    justifyContent: 'center',
  },
})

export default ReceiptItem;

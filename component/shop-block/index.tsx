import React, {useEffect} from "react";

import {
  Pressable,
  StyleSheet,
  View,
  Text,
  ImageBackground,
  ScrollView,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {darkColorSchema} from "../../style/color-schema";
import {setActualScreen} from "../../reducers/commonReducer";
import {
  asyncGetShopCategoryList,
  setActualShopCategory
} from "../../reducers/shopReducer";
import {IShop, IStateShopReducer} from "../../reducers/interface";
import DropDownPicker from "react-native-dropdown-picker";

const ShopBlock = () => {
  const dispatch = useDispatch();

  const shopCategoryAll = useSelector((state: IStateShopReducer) => state.shopReducer.categoryList);

  const setActualScreenAction = () => dispatch(setActualScreen('receipt'));
  const setActualShopCategoryAction = (category: any) => dispatch(setActualShopCategory(category));
  const shopCategoryList = () => dispatch(asyncGetShopCategoryList());

  useEffect(() => {
    shopCategoryList();
  }, [])

  return (
    <View>
      <ScrollView style = {styles.block}>
        <View style = {styles.content}>
          {
            shopCategoryAll.map((item: IShop, index: number) => {
              // const image = {uri: item.image}

              return (
                <Pressable
                  style={styles.oneShop}
                  onPress = {() => [
                    setActualScreenAction(),
                    setActualShopCategoryAction(item)
                  ]}
                  key = {index}
                >
                  {/*<ImageBackground source={image} style={styles.image}>*/}
                  {/*</ImageBackground>*/}
                  <Text style={styles.text}>{item.name}</Text>
                </Pressable>
              )
            })
          }

        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  block: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    width: '100%',
  },
  oneShop: {
    width: 120,
    height: 160,
    margin: 15,
    position: 'relative',
    borderWidth: 1,
  },
  text: {
    backgroundColor: darkColorSchema.complementary,
    bottom: 0,
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
    height: 40,
    padding: 10,
    fontWeight: 'bold'
  },
  image: {
    height: 120,
    justifyContent: 'center',
    borderRadius: 55,
    shadowColor: 'blue',
    shadowOffset: {
      width: 15,
      height: 15,
    },
    shadowOpacity: 50,
    shadowRadius: 50,
  },
});

export default ShopBlock;

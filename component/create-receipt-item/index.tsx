import React, {useState} from "react";
import {
  StyleSheet,
  View,
  Text,
  Pressable, Switch,
} from "react-native";

import {darkColorSchema} from "../../style/color-schema";
import InputElement from "../input-element";
import {setActualScreen} from "../../reducers/commonReducer";
import {useDispatch, useSelector} from "react-redux";
import {IStateReceiptReducer, IStateUnitReducer} from "../../reducers/interface";
import DropDownPicker from "react-native-dropdown-picker";
import {addProduct} from "../../reducers/receiptReducer";

const CreateReceiptItem = () => {
  const dispatch = useDispatch();

  const unitAll = useSelector((state: IStateUnitReducer) => state.unitReducer.unitList);

  const [inputName, setInputName] = useState('');
  const [inputValue, setInputValue] = useState(0);
  const [inputPrice, setInputPrice] = useState(0);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState(unitAll.map((item: any) => ({
      label: item.name,
      value: item.value,
    })));
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  const setActualScreenAction = (screen: string) => dispatch(setActualScreen(screen));
  const draftReceipt = useSelector((state: IStateReceiptReducer) => state.receiptReducer.draftReceipt);

  const addProductItem = (product: any) => {
    product.push({
      name: inputName,
      quantity: inputValue,
      unit: value,
      price: inputPrice,
      strategy: isEnabled,
    });

    dispatch(addProduct(product));
    goToScreen('add_receipt')
  };

  const goToScreen = (screen: string) => setActualScreenAction(screen);

  const changeName =(value: string) => setInputName(value);
  const changeValue =(value: number) => setInputValue(value);
  const changePrice =(value: number) => setInputPrice(value);


  // useEffect(() => {
  //   unitList();
  //   console.log(54321)
  // }, [])

  return (
    <View>
      <View>
        <InputElement type='string' title='Name: ' onChangeText={()=>changeName}/>
        <InputElement type='number' title='Value: ' onChangeText={()=>changeValue}/>
        <View style={styles.unit_block}>
          <Text style={styles.text}>Unit: </Text>
          <DropDownPicker
            style = {styles.dd_picker}
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
          />
        </View>

        <InputElement type='number' title='Price: ' onChangeText={()=>changePrice}/>

        <View
          style = {[
            styles.block_switch
          ]}
        >
          <Text
            style={styles.text_switch}
          >Strategy</Text>
          <View
            style={styles.input_switch}
          >
            <View
              style={styles.wrap_switch}
            >
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>

        </View>
      </View>
      <View>
        <Pressable
          style={styles.button}
          onPress = {() => addProductItem(draftReceipt)}
        >
            <Text style={[styles.text_button, styles.button_add]}>Add</Text>
        </Pressable>

        <Pressable
          style={styles.button}
          onPress = {() => goToScreen('add_receipt')}
        >
          <Text style={[styles.text_button, styles.button_cancel]}>Cancel</Text>
        </Pressable>


      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  button: {
    backgroundColor: darkColorSchema.complementary,
    color: darkColorSchema.text,
    height: 40,
    marginTop: 5,
    borderRadius: 5,
  },

  text_button: {
    width: '100%',
    textAlign: 'center',
    lineHeight: 40,
    color: darkColorSchema.text,
  },

  button_add: {
  },

  button_cancel: {
  },
  unit_block: {
    display: 'flex',
    flexDirection: 'row',
  },
  dd_picker: {
    width: '80%',
  },
  text: {
    width: '20%',
    textAlignVertical: "center",
    padding: 3,
  },

  block_switch: {
    margin: 3,
    height: 50,
    display: 'flex',
    flexDirection: 'row',
  },
  text_switch: {
    width: '20%',
    textAlign: 'left',
    position: 'relative',
    lineHeight: 50,
  },
  input_switch: {
    // flex: 9,
    width: '80%',
    alignContent: "flex-start",
    justifyContent: 'center',
  },
  wrap_switch: {
    // left: 0,
    position: 'absolute',
    borderWidth: 1,
    borderColor: 'green',
  },
})

export default CreateReceiptItem;

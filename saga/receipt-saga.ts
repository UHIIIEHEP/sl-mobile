import {put, takeEvery, call} from 'redux-saga/effects'
import httpHook from "../hook/http.hook";
import {API_RECEIPT_LIST_GET, API_RECEIPT_CREATE_POST} from "../config/url";
import {
  ASYNC_GET_RECEIPT_LIST,
  getReceiptList,
  ASYNC_POST_RECEIPT_CREATE,
} from "../reducers/receiptReducer";

function* receiptWorker (): any {
  const {data} = yield call(httpHook, API_RECEIPT_LIST_GET, 'get');
  const receipt = yield call(() => new Promise(res => res(data.result.shopReceipt)));
  yield put(getReceiptList(receipt))
}

export function* receiptWatcher () {
  yield takeEvery(ASYNC_GET_RECEIPT_LIST, receiptWorker)
}

function* createReceiptWorker (data: any): any {
  yield call(httpHook, API_RECEIPT_CREATE_POST, 'post', data.payload);
}

export function* createReceiptWatcher () {
  yield takeEvery(ASYNC_POST_RECEIPT_CREATE, createReceiptWorker)
}

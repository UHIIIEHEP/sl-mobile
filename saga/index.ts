import {all} from 'redux-saga/effects';
import {shopCategoryWatcher, shopWatcher} from "./shop-saga";
import {createReceiptWatcher, receiptWatcher} from "./receipt-saga";
import {unitWatcher} from "./unit-saga";

export function* rootWatcher() {
  yield all([
    shopWatcher(),
    shopCategoryWatcher(),
    receiptWatcher(),
    createReceiptWatcher(),
    unitWatcher(),
  ])
}
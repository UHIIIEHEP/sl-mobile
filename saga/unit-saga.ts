import {call, put, takeEvery} from 'redux-saga/effects';
import {ASYNC_GET_UNIT_LIST, getUnitList} from '../reducers/unitReducer';
import httpHook from '../hook/http.hook';
import {API_UNIT_LIST_GET} from '../config/url';

const getUnit = () => httpHook(API_UNIT_LIST_GET, 'get');

function* unitWorker(): any {
  const {data} = yield call(getUnit);
  const unit:any = yield call(() => new Promise(res => res(data.result.unit)))
  const result = [];
  for (let key in unit) {
    result.push({
      name: unit[key],
      value: key,
    })
  }
  yield put(getUnitList(result))
}

export function* unitWatcher () {
  yield takeEvery(ASYNC_GET_UNIT_LIST, unitWorker)
}

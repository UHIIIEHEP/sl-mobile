import {put, takeEvery, call} from 'redux-saga/effects'
import httpHook from "../hook/http.hook";
import {
  ASYNC_GET_SHOP_CATEGORY_LIST,
  ASYNC_GET_SHOP_LIST,
  getShopCategoryList,
  getShopList
} from "../reducers/shopReducer";
import {API_SHOP_LIST_GET, API_SHOP_CATEGORY_LIST_GET} from "../config/url";

const getShop = () => httpHook(API_SHOP_LIST_GET, 'get');
const getShopCategory = () => httpHook(API_SHOP_CATEGORY_LIST_GET, 'get');

function* shopWorker (): any {
  const {data} = yield call(getShop);
  const shop:any = yield call(() => new Promise(res => res(data.result.shop)))
  yield put(getShopList(shop))
}

export function* shopWatcher () {
  yield takeEvery(ASYNC_GET_SHOP_LIST, shopWorker)
}

function* shopCategoryWorker (): any {
  const {data} = yield call(getShopCategory);
  const category: any = yield call(() => new Promise(res => res(data.result.category)))
  const result: any = [];
  for (let key in category) {
    result.push({
      name: category[key],
      value: key,
    })
  }
  yield put(getShopCategoryList(result))
}

export function* shopCategoryWatcher () {
  yield takeEvery(ASYNC_GET_SHOP_CATEGORY_LIST, shopCategoryWorker)
}


export interface IStateStyleReducer {
  styleReducer: {
    visibleButtonCreateReceipt: boolean,
    visibleLeftMenu: boolean,
  },
}

export interface IStateCommonReducer {
  commonReducer: {
    menuItems: any,
    actualScreen: string,
    shopItems: string[],
    visibleInputSearch: boolean,
    draftReceipt: object[],
    menuElement: any,
    product: any,
    totalPrice: number,
  },
}

export interface IStateUnitReducer {
  unitReducer: {
    unitList: any,
  },
}

export interface IShop {
  _id: string,
  name: string,
  image: string,
}

export interface IProduct {
  name: string,
  price: number,
  quantity: number,
  unit: string,
}

export interface IReceipt {
  _id: string
  date: string,
  total_price: number
  shop: IShop,
  product: IProduct[]
}

export interface IStateShopReducer {
  shopReducer: {
    shopItems: IShop[],
    actualShop: IShop,
    categoryList: any,
    actualCategory: any,
  },
}

export interface IStateReceiptReducer {
  receiptReducer: {
    receiptList: IReceipt[],
    totalPrice: number,
    draftReceipt: IProduct[],
    payloadReceiptList?: {
      id?: string,
    }
  },
}

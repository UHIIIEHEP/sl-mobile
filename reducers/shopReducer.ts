import {IShop} from "./interface";

const GET_SHOP_LIST = 'GET_SHOP_LIST';
const SET_ACTUAL_SHOP = 'SET_ACTUAL_SHOP';
const GET_SHOP_CATEGORY_LIST = 'GET_SHOP_CATEGORY_LIST';
const SET_ACTUAL_SHOP_CATEGORY = 'SET_ACTUAL_SHOP_CATEGORY';

export const ASYNC_GET_SHOP_LIST = 'ASYNC_GET_SHOP_LIST';
export const ASYNC_GET_SHOP_CATEGORY_LIST = 'ASYNC_GET_SHOP_CATEGORY_LIST';

const defaultState = {
  shopItems: [],
  actualShop: null,
  categoryList: [],
  actualCategory: '',
}

const shopReducer = (state = defaultState, action: any) => {
  switch(action.type) {
    case GET_SHOP_LIST:
      return {
        ...state,
        shopItems: action.payload,
      }
    case SET_ACTUAL_SHOP:
      return {
        ...state,
        actualShop: action.payload,
      }

    case GET_SHOP_CATEGORY_LIST:
      return {
        ...state,
        categoryList: action.payload,
      }
    case SET_ACTUAL_SHOP_CATEGORY:
      return {
        ...state,
        actualCategory: action.payload,
      }

    default:
      return state;
    }
}

export default shopReducer;

export const getShopList = (shop: IShop[]) => ({
  type: GET_SHOP_LIST,
  payload: shop,
})

export const asyncGetShopList = () => ({
  type: ASYNC_GET_SHOP_LIST,
})

export const setActualShop = (shop: IShop | null) => ({
  type: SET_ACTUAL_SHOP,
  payload: shop,
})

export const getShopCategoryList = (category: any) => ({
  type: GET_SHOP_CATEGORY_LIST,
  payload: category,
})

export const asyncGetShopCategoryList = () => ({
  type: ASYNC_GET_SHOP_CATEGORY_LIST,
})

export const setActualShopCategory = (category: string) => ({
  type: SET_ACTUAL_SHOP_CATEGORY,
  payload: category,
})

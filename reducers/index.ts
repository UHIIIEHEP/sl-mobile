import {applyMiddleware, combineReducers} from "redux";
import { createStore} from "redux";

import commonReducer from "./commonReducer";
import createSagaMiddleware from "redux-saga";
import {rootWatcher} from "../saga";
import shopReducer from "./shopReducer";
import receiptReducer from "./receiptReducer";
import unitReducer from "./unitReducer";

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  commonReducer,
  shopReducer,
  receiptReducer,
  unitReducer,
})

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

export default store;

sagaMiddleware.run(rootWatcher);

const SET_ACTUAL_SCREEN = 'SET_ACTUAL_SCREEN';

const defaultState = {
  actualScreen: 'add_receipt',
  colorSchema: 'dark',

  shopItems: [],
  visibleInputSearch: true,
  shopList: null,
}

const commonReducer = (state = defaultState, action: any) => {
  switch(action.type) {
    case SET_ACTUAL_SCREEN:
      return {
        ...state,
        actualScreen: action.payload,
      }

    default:
      return state;
  }
}

export default commonReducer;

export const setActualScreen = (actualScreen: string) => ({
  type: SET_ACTUAL_SCREEN,
  payload: actualScreen,
})

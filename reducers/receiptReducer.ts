import {IReceipt} from "./interface";

const GET_RECEIPT_LIST = 'GET_RECEIPT_LIST';
const ADD_PRODUCT = 'ADD_PRODUCT';
const CLEAR_DRAFT_RECEIPT = 'CLEAR_DRAFT_RECEIPT';

export const ASYNC_GET_RECEIPT_LIST = 'ASYNC_GET_RECEIPT_LIST';
export const ASYNC_POST_RECEIPT_CREATE = 'ASYNC_POST_RECEIPT_CREATE';

const defaultState = {
  receiptList: [],
  payloadReceiptList: {
    id: null,
  },
  // draftReceipt: [],
  draftReceipt: [
    {"name": "Проо", "price": "5095", "strategy": false, "unit": "KILOGRAM", "quantity": "5967"},
    {"name": "Проо", "price": "5095", "strategy": false, "unit": "LITER", "quantity": "5967"},
    {"name": "Проо", "price": "5095", "strategy": true, "unit": "LITER", "quantity": "5967"},
  ],
  totalPrice: 0,
}

const receiptReducer = (state = defaultState, action: any) => {
  switch(action.type) {
    case GET_RECEIPT_LIST:
      return {
        ...state,
        receiptList: action.payload,
      }

    case ADD_PRODUCT:
      return {
        ...state,
        draftReceipt: action.payload.draftReceipt,
        totalPrice: action.payload.totalPrice,
      }

    case CLEAR_DRAFT_RECEIPT:
      return {
        ...state,
        draftReceipt: [],
      }

    default:
      return state;
  }
}

export default receiptReducer;

export const getReceiptList = (receipt: IReceipt[]) => ({
  type: GET_RECEIPT_LIST,
  payload: receipt,
})

export const asyncGetReceiptList = () => ({
  type: ASYNC_GET_RECEIPT_LIST,
})

export const asyncPostReceiptCreate = (payload: any) => ({
  type: ASYNC_POST_RECEIPT_CREATE,
  payload,
})

export const addProduct = (product: any) => {

  let totalPrice = 0;

  product.forEach((item: any) => {
    totalPrice += Number(item.price);
  })

  return ({
    type: ADD_PRODUCT,
    payload: {
      draftReceipt : product,
      totalPrice,
    },
  })
}

export const clearDraftReceipt = () => ({
  type: CLEAR_DRAFT_RECEIPT,
})


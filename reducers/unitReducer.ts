const GET_UNIT_LIST = 'GET_UNIT_LIST';
export const ASYNC_GET_UNIT_LIST = 'ASYNC_GET_UNIT_LIST';

const defaultState = {
  unitList: [],
}

const unitReducer = (state = defaultState, action: any) => {
  switch (action.type) {
    case GET_UNIT_LIST:
      return {
        ...state,
        unitList: action.payload,
      }

    default:
      return state;
  }
}

export default unitReducer;

export const getUnitList = (unit: any) => ({
  type: GET_UNIT_LIST,
  payload: unit,
})

export const asyncGetUnitList = () => ({
  type: ASYNC_GET_UNIT_LIST,
})

import React, {useEffect} from 'react';
import Header from "./component/header";
import {
  StyleSheet,
  View,
  ScrollView,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {darkColorSchema} from "./style/color-schema";
import {
  IStateCommonReducer,
} from "./reducers/interface";

import ShopPage from "./container/shop-page";
import Footer from "./component/footer";
import ReceiptPage from "./container/receipt-page";
import CreateReceiptItemPage from "./container/create-receipt-item-page";
import CreateReceiptPage from "./container/create-receipt-page";
import {asyncGetUnitList} from "./reducers/unitReducer";
import {asyncGetShopCategoryList} from "./reducers/shopReducer";

const App = () => {

  const actualScreen = useSelector((state: IStateCommonReducer) => state.commonReducer.actualScreen)

  const dispatch = useDispatch();
  const shopCategoryList = () => dispatch(asyncGetShopCategoryList());
  const unitList = () => dispatch(asyncGetUnitList());


  useEffect(() => {
    shopCategoryList();
    unitList();
  }, [])

  return (
    <View style = {styles.screen}>
      <Header />
      <View style={styles.content}>

        { (actualScreen === 'receipt') &&
          <ReceiptPage />
        }

        { (actualScreen === 'shop') &&
          <ShopPage />
        }

        { (actualScreen === 'add_receipt') &&
          <CreateReceiptPage />
        }

        { (actualScreen === 'addReceiptItem') &&
          <CreateReceiptItemPage />
        }

        <View style={styles.freeBlock}></View>
      </View>

      <Footer />



    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
  },
  content: {
    width: '100%',
    height: '80%',
    padding: '3%',
    backgroundColor: darkColorSchema.main,
  },
  receipt: {
    height: 250,
    backgroundColor: 'red',
  },

  freeBlock: {
    height: 70,
  },
});

export default App;
